cmake_minimum_required(VERSION 3.10)

include("cmake/HunterGate.cmake")
HunterGate(
    URL "https://github.com/ruslo/hunter/archive/v0.23.207.tar.gz"
    SHA1 "6e1b05a1f65c2aa492f7f43a43578e83ac62bbdd"
    LOCAL # use cmake/Hunter/config.cmake
)

project(thesis)


# Add a custom target to generate all the plots from the numerical experiments
add_custom_target(plots)


hunter_add_package(Eigen)
find_package(Eigen3 CONFIG REQUIRED)

hunter_add_package(GTest)
find_package(GTest CONFIG REQUIRED)

hunter_add_package(Boost COMPONENTS chrono timer system filesystem program_options)
find_package(Boost COMPONENTS system filesystem program_options REQUIRED)

hunter_add_package(lehrfempp)
find_package(lehrfempp CONFIG REQUIRED)


set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")



# Include the GTest directories
include_directories(${GTEST_INCLUDE_DIRS})

# Enable testing
enable_testing()

# Add the different subprojects
add_subdirectory(assemble)
add_subdirectory(mesh)
add_subdirectory(post_processing)
add_subdirectory(examples)
