/**
 * @file nested_cylinders.cc
 * @brief Compute the convergence of the nested cylinders experiment
 */

#include <algorithm>
#include <boost/filesystem.hpp>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <string>

#include <lf/assemble/dofhandler.h>
#include <lf/io/gmsh_reader.h>
#include <lf/io/vtk_writer.h>
#include <lf/mesh/entity.h>
#include <lf/mesh/hybrid2d/mesh_factory.h>
#include <lf/mesh/utils/lambda_mesh_data_set.h>
#include <lf/quad/quad.h>

#include <annulus_triag_mesh_builder.h>
#include <build_system_matrix.h>
#include <norms.h>
#include <piecewise_const_element_matrix_provider.h>
#include <piecewise_const_element_vector_provider.h>
#include <solution_to_mesh_data_set.h>

/**
 * @brief Solve the nested cylinders problem with zero potential at the boundary
 * @param mesh A shared pointer to the mesh on which to solve the PDE
 * @param dofh The dofhandler used for the simulation
 * @param r The radius of the inner cylinder
 * @param R The radius of the outer cylinder
 * @param omega1 The angular velocity of the inner cylinder
 * @param omega2 The angular velocity of the outer cylinder
 * @param modified_penalty If true, use the modified penalty term instead of the
 * original one
 * @returns A vector of basis function coefficients for the solution
 */
Eigen::VectorXd
solveNestedCylindersZeroBC(const std::shared_ptr<const lf::mesh::Mesh> &mesh,
                           const lf::assemble::DofHandler &dofh, double r,
                           double R, double omega1, double omega2,
                           bool modified_penalty) {
  // The volume forces are equal to zero everywhere
  auto f = [](const Eigen::Vector2d &x) { return Eigen::Vector2d::Zero(); };
  // Drive the inner and outer cylinder with omega1 and omega2
  const double eps = 1e-10;
  auto dirichlet_funct = [&](const lf::mesh::Entity &edge) -> Eigen::Vector2d {
    const auto geom = edge.Geometry();
    const auto vertices = geom->Global(edge.RefEl().NodeCoords());
    if (vertices.col(0).norm() <= R + eps &&
        vertices.col(0).norm() >= R - eps &&
        vertices.col(1).norm() <= R + eps && vertices.col(1).norm() >= R - eps)
      return omega2 * R * (vertices.col(1) - vertices.col(0)).normalized();
    if (vertices.col(0).norm() <= r + eps &&
        vertices.col(0).norm() >= r - eps &&
        vertices.col(1).norm() <= r + eps && vertices.col(1).norm() >= r - eps)
      return omega1 * r * (vertices.col(0) - vertices.col(1)).normalized();
    return Eigen::Vector2d::Zero();
  };
  const auto dirichlet =
      *lf::mesh::utils::make_LambdaMeshDataSet(dirichlet_funct);

  // Asemble the LSE
  const auto boundary = lf::mesh::utils::flagEntitiesOnBoundary(mesh);
  // Assemble the Matrix
  lf::assemble::COOMatrix<double> A(dofh.NoDofs(), dofh.NoDofs());
  const thesis::assemble::PiecewiseConstElementMatrixProvider elem_mat_provider(
      100, boundary, modified_penalty);
  lf::assemble::AssembleMatrixLocally(0, dofh, dofh, elem_mat_provider, A);
  // Assemble the right hand side
  Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dofh.NoDofs());
  const thesis::assemble::PiecewiseConstElementVectorProvider elem_vec_provider(
      100, f, lf::quad::make_TriaQR_MidpointRule(), boundary, dirichlet);
  lf::assemble::AssembleVectorLocally(0, dofh, elem_vec_provider, rhs);

  // Enforce the no-flow boundary conditions
  auto selector = [&](lf::base::size_type idx) -> std::pair<bool, double> {
    const auto &entity = dofh.Entity(idx);
    if (entity.RefEl() == lf::base::RefElType::kPoint && boundary(entity))
      return {true, 0};
    return {false, 0};
  };
  lf::assemble::fix_flagged_solution_components(selector, A, rhs);

  // Solve the LSE using sparse LU
  Eigen::SparseMatrix<double> As = A.makeSparse();
  Eigen::SparseLU<Eigen::SparseMatrix<double>> solver(As);
  return solver.solve(rhs);
}

/**
 * @brief Solve the nested cylinders problem with constant potential at the
 * boundary
 * @param mesh A shared pointer to the mesh on which to solve the PDE
 * @param dofh The dofhandler used for the simulation
 * @param r The radius of the inner cylinder
 * @param R The radius of the outer cylinder
 * @param omega1 The angular velocity of the inner cylinder
 * @param omega2 The angular velocity of the outer cylinder
 * @param modified_penalty If true, use the modified penalty term instead of the
 * original one
 * @returns A vector of basis function coefficients for the solution
 */
Eigen::VectorXd
solveNestedCylindersNonzeroBC(const std::shared_ptr<const lf::mesh::Mesh> &mesh,
                              const lf::assemble::DofHandler &dofh, double r,
                              double R, double omega1, double omega2,
                              bool modified_penalty) {
  // The volume forces are equal to zero everywhere
  auto f = [](const Eigen::Vector2d &x) { return Eigen::Vector2d::Zero(); };
  // Drive the inner and outer cylinder with omega1 and omega2
  const double eps = 1e-10;
  auto dirichlet_funct = [&](const lf::mesh::Entity &edge) -> Eigen::Vector2d {
    const auto geom = edge.Geometry();
    const auto vertices = geom->Global(edge.RefEl().NodeCoords());
    if (vertices.col(0).norm() <= R + eps &&
        vertices.col(0).norm() >= R - eps &&
        vertices.col(1).norm() <= R + eps && vertices.col(1).norm() >= R - eps)
      return omega2 * R * (vertices.col(1) - vertices.col(0)).normalized();
    if (vertices.col(0).norm() <= r + eps &&
        vertices.col(0).norm() >= r - eps &&
        vertices.col(1).norm() <= r + eps && vertices.col(1).norm() >= r - eps)
      return omega1 * r * (vertices.col(0) - vertices.col(1)).normalized();
    return Eigen::Vector2d::Zero();
  };
  const auto dirichlet =
      *lf::mesh::utils::make_LambdaMeshDataSet(dirichlet_funct);

  // Asemble the LSE
  const auto boundary = lf::mesh::utils::flagEntitiesOnBoundary(mesh);
  // Assemble the Matrix
  lf::assemble::COOMatrix<double> A(dofh.NoDofs(), dofh.NoDofs());
  const thesis::assemble::PiecewiseConstElementMatrixProvider elem_mat_provider(
      100, boundary, modified_penalty);
  lf::assemble::AssembleMatrixLocally(0, dofh, dofh, elem_mat_provider, A);
  // Assemble the right hand side
  Eigen::VectorXd rhs = Eigen::VectorXd::Zero(dofh.NoDofs());
  const thesis::assemble::PiecewiseConstElementVectorProvider elem_vec_provider(
      100, f, lf::quad::make_TriaQR_MidpointRule(), boundary, dirichlet);
  lf::assemble::AssembleVectorLocally(0, dofh, elem_vec_provider, rhs);

  // Combine the basis functions on the inside boundary to a single one
  const lf::base::size_type M_outer = 0;
  const lf::base::size_type M_inner = 1;
  // Create a mapping of DOF indices to remove the afterwards unused DOFs
  std::vector<lf::base::size_type> dofmap(dofh.NoDofs());
  lf::base::size_type idx = 2;
  for (lf::base::size_type dof = 0; dof < dofh.NoDofs(); ++dof) {
    const auto &entity = dofh.Entity(dof);
    const auto geom = entity.Geometry();
    if (entity.RefEl() == lf::base::RefElType::kPoint && boundary(entity)) {
      if (geom->Global(entity.RefEl().NodeCoords()).norm() > R - eps)
        dofmap[dof] = M_outer;
      else
        dofmap[dof] = M_inner;
    } else
      dofmap[dof] = idx++;
  }
  // Apply this mapping to the triplets of the matrix
  std::for_each(A.triplets().begin(), A.triplets().end(),
                [&](Eigen::Triplet<double> &trip) {
                  trip = Eigen::Triplet<double>(
                      dofmap[trip.row()], dofmap[trip.col()], trip.value());
                });
  // Apply the mapping to the right hand side vector
  Eigen::VectorXd rhs_mapped = Eigen::VectorXd::Zero(idx);
  for (lf::base::size_type dof = 0; dof < dofh.NoDofs(); ++dof)
    rhs_mapped[dofmap[dof]] += rhs[dof];

  // Set the potential on the outer boundary to zero
  auto selector = [&](lf::base::size_type idx) -> std::pair<bool, double> {
    return {idx == M_outer, 0};
  };
  lf::assemble::fix_flagged_solution_components(selector, A, rhs);

  // Solve the LSE using sparse LU
  Eigen::SparseMatrix<double> As_mapped = A.makeSparse().block(0, 0, idx, idx);
  Eigen::SparseLU<Eigen::SparseMatrix<double>> solver(As_mapped);
  // Eigen::SparseQR<Eigen::SparseMatrix<double>, Eigen::COLAMDOrdering<int>>
  // solver(As);
  const Eigen::VectorXd sol_mapped = solver.solve(rhs_mapped);

  // Apply the inverse mapping to recovr the basis function coefficients for the
  // original basis functions
  Eigen::VectorXd sol = Eigen::VectorXd::Zero(dofh.NoDofs());
  for (lf::base::size_type dof = 0; dof < dofh.NoDofs(); ++dof)
    sol[dof] = sol_mapped[dofmap[dof]];

  return sol;
}

/**
 * @brief Concatenate objects defining an operator<<(std::ostream&)
 * @param args A variadic pack of objects implementing
 * `operator<<(std::ostream&)`
 * @returns A string with the objects concatenated
 */
template <typename... Args> static std::string concat(Args &&... args) {
  std::ostringstream ss;
  (ss << ... << args);
  return ss.str();
}

/**
 * @brief outputs the L2 and DG norm errors for the nested cylinders experiment
 *
 * Three different command line arguments can be provided:
 *   - builder
 *   - files
 *   - irregular
 *
 * Providing builder as a command line argument will build the meshes with
 * #AnnulusTriagMeshBuilder. Providing files as a command line argument will use
 * uniform meshes generated by GMSH. Providing irregular as a command line
 * argument will use meshes with a sudden jump in mesh resolution.
 */
int main(int argc, char *argv[]) {
  const double r = 0.25;
  const double R = 1;
  const double omega1 = 0;
  const double omega2 = 1;

  if (argc != 2) {
    std::cerr << "Usage: " << argv[0] << "[builder,files,irregular]"
              << std::endl;
    std::cerr << "\tbuilder  : Builds the meshes with a AnnulusTriagMeshBuilder"
              << std::endl;
    std::cerr << "\tfiles    : Reads the meshes from the files annulus??.msh"
              << std::endl;
    std::cerr << "\tirregular: Solves the PDE on a single mesh with a sudden "
                 "change in resolution"
              << std::endl;
    exit(1);
  }

  std::vector<std::shared_ptr<lf::mesh::Mesh>> meshes;
  if (strcmp(argv[1], "files") == 0) {
    // Read the mesh from the gmsh file
    boost::filesystem::path meshpath = __FILE__;
    meshpath = meshpath.parent_path();
    for (int i = 0; i <= 4; ++i) {
      const auto meshfile = meshpath / concat("annulus", std::setw(2),
                                              std::setfill('0'), i, ".msh");
      std::unique_ptr<lf::mesh::MeshFactory> factory =
          std::make_unique<lf::mesh::hybrid2d::MeshFactory>(2);
      lf::io::GmshReader reader(std::move(factory), meshfile.string());
      meshes.push_back(reader.mesh());
    }
  }
  if (strcmp(argv[1], "builder") == 0) {
    // Build a sequence of meshes
    for (int i = 0; i < 8; ++i) {
      const unsigned nx = 4u << i;
      const double dx = 2 * M_PI * (r + R) / 2 / nx;
      const unsigned ny = std::max(static_cast<unsigned>((R - r) / dx), 1u);

      // Build the mesh
      std::shared_ptr<lf::mesh::MeshFactory> factory =
          std::make_shared<lf::mesh::hybrid2d::MeshFactory>(2);
      thesis::mesh::AnnulusTriagMeshBuilder builder(factory);
      builder.setBottomLeftCorner(0, r);
      builder.setTopRightCorner(1, R);
      builder.setNoXCells(nx);
      builder.setNoYCells(ny);
      meshes.push_back(builder.Build());
    }
  }
  if (strcmp(argv[1], "irregular") == 0) {
    boost::filesystem::path meshpath = __FILE__;
    const auto mesh_irregular_path =
        meshpath.parent_path() / "annulus_irregular.msh";
    const auto mesh_irregular_inverted_path =
        meshpath.parent_path() / "annulus_irregular_inverted.msh";
    std::unique_ptr<lf::mesh::MeshFactory> factory_irregular =
        std::make_unique<lf::mesh::hybrid2d::MeshFactory>(2);
    std::unique_ptr<lf::mesh::MeshFactory> factory_irregular_inverted =
        std::make_unique<lf::mesh::hybrid2d::MeshFactory>(2);
    lf::io::GmshReader reader_irregular(std::move(factory_irregular),
                                        mesh_irregular_path.string());
    lf::io::GmshReader reader_irregular_inverted(
        std::move(factory_irregular_inverted),
        mesh_irregular_inverted_path.string());
    meshes.push_back(reader_irregular.mesh());
    meshes.push_back(reader_irregular_inverted.mesh());
  }

  // Compute the analytic solution of the problem
  const double C1 = 2 * (omega1 * r * r - omega2 * R * R) / (r * r - R * R);
  const double C2 = ((omega1 - omega2) * r * r * R * R) / (R * R - r * r);
  auto analytic_velocity = [&](const Eigen::Vector2d &x) -> Eigen::Vector2d {
    const double radius = x.norm();
    Eigen::Vector2d vec;
    vec << x[1], -x[0];
    vec.normalize();
    return -(0.5 * C1 * radius + C2 / radius) * vec;
  };
  auto analytic_gradient = [&](const Eigen::Vector2d &x) -> Eigen::Matrix2d {
    const double r2 = x.squaredNorm();
    Eigen::Matrix2d g;
    g << 2 * C2 * x[0] * x[1] / r2 / r2,
        -C1 / 2 - (C2 * r2 - 2 * C2 * x[1] * x[1]) / r2 / r2,
        C1 / 2 + (C2 * r2 - 2 * C2 * x[0] * x[0]) / r2 / r2,
        -2 * C2 * x[0] * x[1] / r2 / r2;
    return g;
  };

  // Solve the problem on each mesh and compute the error
  for (const auto mesh : meshes) {
    lf::assemble::UniformFEDofHandler dofh(
        mesh,
        {{lf::base::RefEl::kPoint(), 1}, {lf::base::RefEl::kSegment(), 1}});
    const Eigen::VectorXd solution_zero =
        solveNestedCylindersZeroBC(mesh, dofh, r, R, omega1, omega2, false);
    const Eigen::VectorXd solution_nonzero =
        solveNestedCylindersNonzeroBC(mesh, dofh, r, R, omega1, omega2, false);
    const Eigen::VectorXd solution_zero_modified =
        solveNestedCylindersZeroBC(mesh, dofh, r, R, omega1, omega2, true);
    const Eigen::VectorXd solution_nonzero_modified =
        solveNestedCylindersNonzeroBC(mesh, dofh, r, R, omega1, omega2, true);
    const auto velocity_zero =
        thesis::post_processing::extractVelocity(mesh, dofh, solution_zero);
    const auto velocity_nonzero =
        thesis::post_processing::extractVelocity(mesh, dofh, solution_nonzero);
    const auto velocity_zero_modified =
        thesis::post_processing::extractVelocity(mesh, dofh,
                                                 solution_zero_modified);
    const auto velocity_nonzero_modified =
        thesis::post_processing::extractVelocity(mesh, dofh,
                                                 solution_nonzero_modified);
    // Store the solution
    lf::io::VtkWriter writer_zero(
        mesh, concat("result_zero_", dofh.NoDofs(), ".vtk"));
    lf::io::VtkWriter writer_nonzero(
        mesh, concat("result_nonzero_", dofh.NoDofs(), ".vtk"));
    writer_zero.WriteCellData("velocity", velocity_zero);
    writer_zero.WriteCellData("velocity_modified", velocity_zero_modified);
    writer_nonzero.WriteCellData("velocity", velocity_nonzero);
    writer_nonzero.WriteCellData("velocity_modified",
                                 velocity_nonzero_modified);
    auto analytic = [&](const lf::mesh::Entity &entity) -> Eigen::Vector2d {
      const Eigen::Vector2d center = entity.Geometry()
                                         ->Global(entity.RefEl().NodeCoords())
                                         .rowwise()
                                         .sum() /
                                     entity.RefEl().NumNodes();
      return analytic_velocity(center);
    };
    writer_zero.WriteCellData(
        "analytic", *lf::mesh::utils::make_LambdaMeshDataSet(analytic));
    writer_nonzero.WriteCellData(
        "analytic", *lf::mesh::utils::make_LambdaMeshDataSet(analytic));
    // Compute the difference between the numerical and the analytical solution
    auto diff_velocity_zero = [&](const lf::mesh::Entity &cell,
                                  const Eigen::Vector2d &x) -> Eigen::Vector2d {
      return velocity_zero(cell) - analytic_velocity(x);
    };
    auto diff_velocity_zero_modified =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Vector2d {
      return velocity_zero_modified(cell) - analytic_velocity(x);
    };
    auto diff_velocity_nonzero =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Vector2d {
      return velocity_nonzero(cell) - analytic_velocity(x);
    };
    auto diff_velocity_nonzero_modified =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Vector2d {
      return velocity_nonzero_modified(cell) - analytic_velocity(x);
    };
    auto diff_gradient_zero = [&](const lf::mesh::Entity &cell,
                                  const Eigen::Vector2d &x) -> Eigen::Matrix2d {
      return -analytic_gradient(x);
    };
    auto diff_gradient_zero_modified =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Matrix2d {
      return -analytic_gradient(x);
    };
    auto diff_gradient_nonzero =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Matrix2d {
      return -analytic_gradient(x);
    };
    auto diff_gradient_nonzero_modified =
        [&](const lf::mesh::Entity &cell,
            const Eigen::Vector2d &x) -> Eigen::Matrix2d {
      return -analytic_gradient(x);
    };
    const double L2_zero =
        thesis::post_processing::L2norm(mesh, diff_velocity_zero, 10);
    const double L2_nonzero =
        thesis::post_processing::L2norm(mesh, diff_velocity_nonzero, 10);
    const double DG_zero = thesis::post_processing::DGnorm(
        mesh, diff_velocity_zero, diff_gradient_zero, 10);
    const double DG_nonzero = thesis::post_processing::DGnorm(
        mesh, diff_velocity_nonzero, diff_gradient_nonzero, 10);
    const double L2_zero_modified =
        thesis::post_processing::L2norm(mesh, diff_velocity_zero_modified, 10);
    const double L2_nonzero_modified = thesis::post_processing::L2norm(
        mesh, diff_velocity_nonzero_modified, 10);
    const double DG_zero_modified = thesis::post_processing::DGnorm(
        mesh, diff_velocity_zero_modified, diff_gradient_zero_modified, 10);
    const double DG_nonzero_modified =
        thesis::post_processing::DGnorm(mesh, diff_velocity_nonzero_modified,
                                        diff_gradient_nonzero_modified, 10);
    std::cout << mesh->NumEntities(2) << ' ' << L2_zero << ' ' << DG_zero << ' '
              << L2_nonzero << ' ' << DG_nonzero << ' ' << L2_zero_modified
              << ' ' << DG_zero_modified << ' ' << L2_nonzero_modified << ' '
              << DG_nonzero_modified << std::endl;
  }

  return 0;
}
