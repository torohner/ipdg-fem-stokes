var searchData=
[
  ['piecewiseboundarynormaljumpassembler',['PiecewiseBoundaryNormalJumpAssembler',['../classthesis_1_1assemble_1_1PiecewiseBoundaryNormalJumpAssembler.html#abfef44cc228cb3b5427410e891a1fab5',1,'thesis::assemble::PiecewiseBoundaryNormalJumpAssembler']]],
  ['piecewiseconstelementmatrixprovider',['PiecewiseConstElementMatrixProvider',['../classthesis_1_1assemble_1_1PiecewiseConstElementMatrixProvider.html#a65b26e40acf01f4c9cb0052867c241f2',1,'thesis::assemble::PiecewiseConstElementMatrixProvider']]],
  ['piecewiseconstelementvectorprovider',['PiecewiseConstElementVectorProvider',['../classthesis_1_1assemble_1_1PiecewiseConstElementVectorProvider.html#abffda87cbd422a372d24e22e32f83b76',1,'thesis::assemble::PiecewiseConstElementVectorProvider']]],
  ['poiseuillevelocity',['poiseuilleVelocity',['../poiseuille_8cc.html#afae79e910f405c12bc072d85d26c1780',1,'poiseuilleVelocity(double h, double flowrate, double y):&#160;poiseuille.cc'],['../step_8cc.html#afae79e910f405c12bc072d85d26c1780',1,'poiseuilleVelocity(double h, double flowrate, double y):&#160;step.cc']]]
];
